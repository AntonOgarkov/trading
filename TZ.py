import numpy
import talib
import requests
import json
import time
import urllib, http.client
import hmac, hashlib
from urllib.parse import urlparse
import datetime as datetime
import bitmex

API_KEY = 'p4tNHWiIWF3HH1_vMYeOodh0'
API_SECRET = 'KzFkOK7741GTpxIjidBTwMbAqTFUium81VxxDphpfqxVCoM8'
symbol_bin = 'TRXBTC'
symbol_bit1 = 'XBTUSD'
symbol_bit2 = 'TRXZ19'
amountUSD = 1
client = bitmex.bitmex(test=False, api_key=API_KEY, api_secret=API_SECRET)

# данный код получился не совсем оптимальным, тк для меня тема торгов на бирже была совершенно новой и я не понимал по началу многое

def start_long(amount = amountUSD):
    print('long')
    # try except при каждом вызове нужны для того чтобы скрипт не перестал работать при каждом вызове
    try:
        data = requests.get(f'https://api.binance.com/api/v1/klines?symbol={symbol_bin}&interval=1m&limit=1').json()
    except:
        time.sleep(10)
        data = requests.get(f'https://api.binance.com/api/v1/klines?symbol={symbol_bin}&interval=1m&limit=1').json()
    start = data[0][4]
    try:
        client.Order.Order_new(symbol=symbol_bit1, orderQty=1).result()
    except:
        time.sleep(10)
        client.Order.Order_new(symbol=symbol_bit1, orderQty=1).result()
    try:
        client.Order.Order_new(symbol=symbol_bit2, orderQty=-1).result()
    except:
        time.sleep(10)
        client.Order.Order_new(symbol=symbol_bit2, orderQty=-1).result()

    while(True):
        time.sleep(60)
        try:
            data = requests.get(f'https://api.binance.com/api/v1/klines?symbol={symbol_bin}&interval=1m&limit=1').json()
        except:
            time.sleep(10)
            data = requests.get(f'https://api.binance.com/api/v1/klines?symbol={symbol_bin}&interval=1m&limit=1').json()
        nowPrice = data[0][4]
        lastLast, last, val = get_dayly()
        print(nowPrice, start)
        if float(nowPrice) > float(start)*1.2:

            try:
                client.Order.Order_new(symbol=symbol_bit1, orderQty=-1).result()
            except:
                time.sleep(10)
                client.Order.Order_new(symbol=symbol_bit1, orderQty=-1).result()
            try:
                client.Order.Order_new(symbol=symbol_bit2, orderQty=1).result()
            except:
                time.sleep(10)
                client.Order.Order_new(symbol=symbol_bit2, orderQty=1).result()
            print('profit')
            return True
        elif float(val)<=float(nowPrice):
            try:
                client.Order.Order_new(symbol=symbol_bit1, orderQty=-1).result()
            except:
                time.sleep(10)
                client.Order.Order_new(symbol=symbol_bit1, orderQty=-1).result()
            try:
                client.Order.Order_new(symbol=symbol_bit1, orderQty=1).result()
            except:
                time.sleep(10)
                client.Order.Order_new(symbol=symbol_bit1, orderQty=1).result()
            print('stop')
            return False



def start_short(amount = amountUSD):
    print('short')
    try:
        data = requests.get(f'https://api.binance.com/api/v1/klines?symbol={symbol_bin}&interval=1m&limit=1').json()
        start = data[0][4]
        client.Order.Order_new(symbol=symbol_bit1, orderQty=-1).result()
    except:
        time.sleep(10)
        data = requests.get(f'https://api.binance.com/api/v1/klines?symbol={symbol_bin}&interval=1m&limit=1').json()
        start = data[0][4]
        client.Order.Order_new(symbol=symbol_bit1, orderQty=-1).result()
    try:
        client.Order.Order_new(symbol=symbol_bit1, orderQty=1).result()
    except:
        time.sleep(10)
        client.Order.Order_new(symbol=symbol_bit1, orderQty=1).result()
    while(True):
        time.sleep(60)
        lastLast, last, val = get_dayly()
        try:
            data = requests.get(f'https://api.binance.com/api/v1/klines?symbol={symbol_bin}&interval=1m&limit=1').json()
        except:
            time.sleep(10)
            data = requests.get(f'https://api.binance.com/api/v1/klines?symbol={symbol_bin}&interval=1m&limit=1').json()
        nowPrice = data[0][4]
        print(nowPrice, start)
        if float(nowPrice) < float(start)*0.833:

            try:
                client.Order.Order_new(symbol=symbol_bit1, orderQty=1).result()
            except:
                time.sleep(10)
                client.Order.Order_new(symbol=symbol_bit1, orderQty=1).result()
            try:
                client.Order.Order_new(symbol=symbol_bit2, orderQty=-1).result()
            except:
                time.sleep(10)
                client.Order.Order_new(symbol=symbol_bit2, orderQty=-1).result()
            print('profit')
            return True
        elif float(val)>=float(nowPrice):
            try:
                client.Order.Order_new(symbol=symbol_bit1, orderQty=1).result()
            except:
                time.sleep(10)
                client.Order.Order_new(symbol=symbol_bit1, orderQty=1).result()
            try:
                client.Order.Order_new(symbol=symbol_bit1, orderQty=-1).result()
            except:
                time.sleep(10)
                client.Order.Order_new(symbol=symbol_bit1, orderQty=-1).result()
            print('stop')
            return False



def get_dayly():
    try:
        data = requests.get(f'https://api.binance.com/api/v1/klines?symbol={symbol_bin}&interval=1m&limit=40').json()
    except:
        time.sleep(10)
        data = requests.get(f'https://api.binance.com/api/v1/klines?symbol={symbol_bin}&interval=1m&limit=40').json()
    # print(data)
    quotes = {}
    quotes['open'] = numpy.asarray([float(item[1]) for item in data])
    quotes['high'] = numpy.asarray([float(item[2]) for item in data])
    quotes['low'] = numpy.asarray([float(item[3]) for item in data])
    quotes['close'] = numpy.asarray([float(item[4]) for item in data])
    return  quotes['close'][-3],quotes['close'][-2],talib.MA(quotes['close'], timeperiod=39, matype=1)[-2]

lastLast,last,val = get_dayly()
f = False
while(True):
    # data = requests.get("https://www.binance.com/api/v1/time").json()
    # now = datetime.datetime.utcfromtimestamp(float(data['serverTime'] / 1000)).date()
    # if now != date:
    date = datetime.datetime.now().date()
    lastLast,last,val = get_dayly()
    print(lastLast,last,val)
    if (val < last and val<lastLast) or (f and val < last):
        f = start_short()
    elif val > last and val>lastLast:
        f = start_long()
    time.sleep(60)
