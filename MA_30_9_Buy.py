import numpy
import talib
import requests
import json
import time
import urllib, http.client
import hmac, hashlib
from urllib.parse import urlparse
import datetime as datetime
API_KEY = 'XQ9DC5GB-DU07KOIH-6MSSTJD7-FWGWLGR8'
API_SECRET = b'fb10319e7aa371f9eb7dc8e389efe8870ef21d449fdbfa5996b8b9c764c4067ebc54594c7020db9dfa49223f0d820fd2f51c937e95c4a15a0810e39856b209c4'
stop = time.time() + 60*60
now = time.time()

def call_api(api_url='https://poloniex.com/tradingApi', http_method="POST", **kwargs):
    time.sleep(0.2) # По правилам биржи нельзя больше 6 запросов в секунду
    payload = {'nonce': int(round(time.time()*1000))}

    if kwargs:
        payload.update(kwargs)
    payload = urllib.parse.urlencode(payload)

    H = hmac.new(key=API_SECRET, digestmod=hashlib.sha512)
    H.update(payload.encode('utf-8'))
    sign = H.hexdigest()

    headers = {"Content-type": "application/x-www-form-urlencoded",
           "Key":API_KEY,
           "Sign":sign}

    url_o = urlparse(api_url)
    conn = http.client.HTTPSConnection(url_o.netloc)
    conn.request(http_method, api_url, payload, headers)
    response = conn.getresponse().read()

    conn.close()

    try:
        obj = json.loads(response.decode('utf-8'))

        if 'error' in obj and obj['error']:
            print(obj['error'])
        return obj
    except ValueError:
        print('ValueError')

def eth_cource():
    resource = requests.get('https://min-api.cryptocompare.com/data/price?fsym=USD&tsyms=ETH').text
    return float(resource.split(':')[1][:-1])

def buy(bye_rate):
    call_api(command='buy', currencyPair='BTC_ETH', amount=eth_cource(), rate=bye_rate)
    print('buy')

def sell(bye_rate):
    call_api(command='sell', currencyPair='BTC_ETH', amount=eth_cource(), rate=bye_rate)
    print('sell')


# оптимизация - сделать запросы по 5 мин и добавлять в массив по одному элементу
# + талиб странно считает работает только при отрезке 4ч, а должен при 50 мин.

# while(now<stop):
while (True):
    now = time.time()
    start_time = now-4*60*60
    resource = requests.get("https://poloniex.com/public?command=returnChartData&currencyPair=BTC_ETH&start=%s&end=9999999999&period=300" % start_time)
    data = json.loads(resource.text)
    # print(data)
    quotes = {}
    quotes['open']  = numpy.asarray([item['open'] for item in data])
    quotes['close'] = numpy.asarray([item['close'] for item in data])
    quotes['high']  = numpy.asarray([item['high'] for item in data])
    quotes['low']   = numpy.asarray([item['low'] for item in data])
    xdate=[datetime.datetime.fromtimestamp(item['date']) for item in data]
    if (quotes['close'][-1]<quotes['open'][-1]):
        if talib.MA(quotes['close'], timeperiod=9, matype=1)[-1] >= quotes['close'][-1] and talib.MA(quotes['close'], timeperiod=9, matype=1)[-1] < quotes['open'][-1]:
            print('try sell')
            sell(quotes['close'])
    else:
        if talib.MA(quotes['close'], timeperiod=30, matype=1)[-1] <= quotes['close'][-1] and talib.MA(quotes['close'], timeperiod=30, matype=1)[-1] > quotes['open'][-1]:
            print('try buy')
            buy(quotes['close'])
